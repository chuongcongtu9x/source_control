import { Component, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  form: FormGroup | undefined;
  constructor(private fb: FormBuilder) {
    this.initFormGroupTestConfflit();
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  
  }

  /**
   * Init form data.
   * @returns FormGroup
   * @author PhiLv2
   */
  private initFormGroupTestConfflit(): void {
    this.form = this.fb.group({
      note: [''],
      // Lich su khong hoan thanh.
      historyContract: this.fb.group({
        isChecked: [false],
        startYear: [null, [Validators.required, Validators.max(moment().year())]]
      }),
      // Doanh thu binh quan hang nam.
      averageRevenue: this.fb.group({
        isChecked: [false],
        totalRevenueMin: [null, [Validators.required, Validators.min(1)]],
        revenueYear: [null, [Validators.required, Validators.min(1), Validators.max(10)]]
      }),
      contractExperience: this.fb.group({
        isChecked: [false],
        experienceYear: ['', [Validators.required]], // năm
        contractValue: [null, [Validators.required]], // giá trị hợp đồng tối thiểu
        similarProperties: [null, [Validators.required, Validators.maxLength(10000)]] // tính chất tương tự
      }),
      insurance: this.fb.group({
        isChecked: [false],
      }),
      quantity: [null, [Validators.required, Validators.maxLength(10000)]],
      insuranceTable2: this.fb.group({
        isChecked: [false],
      insurance1: this.fb.group({
        isChecked1: [false],
      }),
      quantity: [null, [Validators.required, Validators.maxLength(10000)]],
      insuranceTable2: this.fb.group({
        isChecked1: [false],
      }),
    });
    // this.checkedValueChangeDTRR();
    // this.checkedValueChangeCHCT();
  }
}